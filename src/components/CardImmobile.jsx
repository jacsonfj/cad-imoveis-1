import React from 'react';
import PropTypes from 'prop-types';

import { Card, ImgContainer, Describe } from './Card';

const CardImmobbile = props => (
  <Card>
    <ImgContainer>
      <img src="assets/fachada.jpg" alt="Foto Imóvel" />
      <span>
        {props.value}
        <br />
        {props.type}
      </span>
    </ImgContainer>
    <Describe>
      <span>{props.title}</span><span>{props.location}</span>
    </Describe>
  </Card>
);

CardImmobbile.propTypes = {
  value: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
};

export default CardImmobbile;
