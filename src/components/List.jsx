import styled from 'styled-components';

export const List = styled.ul`
  margin-top: 30px;
  flex-direction: column;
  padding: 0px;
`;

export const Item = styled.li`
  display: flex;
  &:hover{
    background-color: #515151;
  }
`;