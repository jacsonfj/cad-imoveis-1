import styled from 'styled-components';

export const Card = styled.div`
  margin: 0.8% 0% 0% 0.8%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  width: 24%;
  border: #eee solid 1px;
`;

export const ImgContainer = styled.div`
  width: 100%;
  padding-top: 75%;
  position: relative;
  &>img{
    position: absolute;
    left: 0;
    top: 0;
    object-fit: cover;
    width: 100%;
    height: 100%;
  }
  &>span{
    font-weight: bold;
    color: #339b70;
    position: absolute;
    right: 12px;
    bottom: 12px;
  }
`;

export const Describe = styled.p`
  margin: 12px;
  display: flex;
  flex-direction: column;
  &>span:first-child{
    font-size: 1em;
    font-weight: bold;
  }
  &>>span:last-child{
    font-size: 0.8em;
    color: #aaa;
    margin-top: 5px;
  }
`;