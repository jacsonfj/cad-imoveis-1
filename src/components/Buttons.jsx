import styled from 'styled-components';

const Button = styled.button`
  cursor: pointer;
  outline: none;
  transition: filter 300ms, transform 200ms;
  color: white;
  border: none;
  font-size: 0.85em;
  padding: 0.85em;
  border-radius: 5px;
  margin-left: ${props => props.marginLeft || '0px'};

  &:hover{
    filter: brightness(120%);
  }
  &:active{
    transform: scale(0.9);
  }
`;

export const ButtonConfirm = Button.extend`
  background-color: #05be7e;
`;

export const ButtonReject = Button.extend`
  background-color: #d84938;
`;
