import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { ButtonConfirm } from './Buttons';

const TopHeader = styled.header.attrs({
  className: 'col s16',
})`
  justify-content: space-between;
  align-items: center;
  background-color: #ddd;
  padding: 1.2em;
`;
const Title = styled.h1`
  font-size: 1.2em;
`;

const Header = props => (
  <TopHeader>
    <Title>{ props.title }</Title>
    { props.btn && (
      <ButtonConfirm onClick={props.btn.onClick}>{ props.btn.title }</ButtonConfirm>
    )}
  </TopHeader>
);

Header.propTypes = {
  title: PropTypes.string.isRequired,
  btn: PropTypes.shape({
    title: PropTypes.string,
    onClick: PropTypes.func,
  }),
};
Header.defaultProps = {
  btn: null,
};

export default Header;
