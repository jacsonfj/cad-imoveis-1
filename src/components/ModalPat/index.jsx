import React from 'react';
import Modal from 'react-modal';
import PropTypes from 'prop-types';

import { ModalHeader, customStyles, ModalContent, ModalFooter, ModalTitle, ModalForm } from './styles';

Modal.setAppElement('#root');

const propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onAfterOpen: PropTypes.func.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  shouldCloseOnEsc: PropTypes.bool,
  onSubmit: PropTypes.func,
  width: PropTypes.string,
  title: PropTypes.node.isRequired,
  footer: PropTypes.node.isRequired,
  children: PropTypes.node.isRequired,
};
const defaultProps = {
  shouldCloseOnEsc: true,
  onSubmit: undefined,
  width: '700px',
};

class ModalPat extends React.Component {
  constructor(props) {
    super(props);
    this.customStyles = customStyles;
    if (props.width) this.customStyles.width = props.width;
  }

  getPatContent() {
    return (
      <React.Fragment>
        <ModalHeader>
          <ModalTitle>{this.props.title}</ModalTitle>
        </ModalHeader>
        <ModalContent>{this.props.children}</ModalContent>
        <ModalFooter>{this.props.footer}</ModalFooter>
      </React.Fragment>
    );
  }

  getContent() {
    if (this.props.onSubmit) {
      return <ModalForm onSubmit={this.props.onSubmit}>{this.getPatContent()}</ModalForm>;
    }
    return this.getPatContent();
  }

  render() {
    return (
      <Modal
        isOpen={this.props.isOpen}
        onAfterOpen={this.props.onAfterOpen}
        onRequestClose={this.props.onRequestClose}
        shouldCloseOnEsc={this.props.shouldCloseOnEsc}
        style={this.customStyles}
        contentLabel="Modal"
      >
        {this.getContent()}
      </Modal>
    );
  }
}

ModalPat.propTypes = propTypes;
ModalPat.defaultProps = defaultProps;

export default ModalPat;
