import styled from 'styled-components';

export const ModalHeader = styled.header`
  padding: 1.2em;
  align-items: center;
  position: relative;
  
  &::after{
    content: '';
    position: absolute;
    left: 1.2em;
    bottom: 0;
    background-color: #eee;
    width: calc(100% - 2.4em);
    height: 1px;
  }
`;

export const ModalTitle = styled.h1`
  font-size: 1.2em;
`;

export const ModalContent = styled.div`
  padding: 1.2em;
`;

export const ModalFooter = styled.footer`
  padding: 1.2em;
  display: flex;
  justify-content: flex-end;
  background-color: #ededed;
`;

export const ModalForm = styled.form`
  padding: 0px !important;
  flex-direction: column;
`;

export const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    maxHeight: '90vh',
    maxWidth: '90vw',
    width: '700px',
    padding: '0px !important',
    flexDirection: 'column',
  },
};
