import React from 'react';
import PropTypes from 'prop-types';

import { FormField, Input } from './styles';

const propTypes = {
  fields: PropTypes.shape({
    label: PropTypes.string,
    input: PropTypes.object,
  }).isRequired,
  prefix: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

const FormGroup = props => (
  <React.Fragment>
    {Object.keys(props.fields).map(name => (
      <FormField key={props.prefix + name} htmlFor={props.prefix + name} className={props.fields[name].label.className}>
        {props.fields[name].label.title}
        <Input id={props.prefix + name} onChange={props.onChange} name={name} {...props.fields[name].input} />
      </FormField>
    ))}
  </React.Fragment>
);

FormGroup.propTypes = propTypes;

export default FormGroup;
