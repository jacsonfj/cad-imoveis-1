import styled from 'styled-components';

export const FormField = styled.label`
  display: flex;
  flex-direction: column;
  padding: 10px;
  color: #5d5d5d;
  font-size: 0.9em;
`;

export const Input = styled.input.attrs({ type: props => props.type || 'text' })`
  background-color: #ededed;
  border: 1px solid #aaa;
  padding: 0.5em;
  font-size: 0.9em;
  margin-top: 5px;
`;
