import React from 'react';
import { Link } from 'react-router-dom';
import MdHome from 'react-icons/lib/md/home';
import MdDashBoard from 'react-icons/lib/md/dashboard';
import styled from 'styled-components';

import { List, Item } from './List';

const Side = styled.aside.attrs({ className: 'full-wd col s3' })`
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  background-color: #3e3e3e;
  color: white;
  padding: 30px 0px;
`;
const ListLink = styled(Link)`
  padding: 8px 1.2em;
  align-items: center;
  & >svg{
    transform: scale(1.1);
    margin-right: 1.2em;
  }
`;

const Sidebar = () => (
  <Side>
    <nav className="col s16 center">
      <img className="s8" src="assets/logo-tecimob.svg" alt="logo" />
      <List className="col s16">
        <Item>
          <ListLink to="/"><MdDashBoard /> Início</ListLink>
        </Item>
        <Item>
          <ListLink to="/imoveis"><MdHome /> Imóveis</ListLink>
        </Item>
      </List>
    </nav>
    <footer className="col s16 center">2015 - Tecimob</footer>
  </Side>
);

export default Sidebar;
