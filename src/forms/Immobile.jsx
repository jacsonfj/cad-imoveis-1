import React from 'react';
import PropTypes from 'prop-types';

import { ButtonConfirm, ButtonReject } from '../components/Buttons';
import ModalPat from '../components/ModalPat';
import FormGroup from '../components/FormGroup';

class Immobile extends React.Component {
  constructor() {
    super();
    this.emptyForm = {
      title: {
        input: { value: '', placeholder: 'Digite o título do imóvel', required: true },
        label: { title: 'Título', className: 'col s8' },
      },
      location: {
        input: { value: '', placeholder: 'Digite a localização do imóvel', required: true },
        label: { title: 'Localização', className: 'col s8' },
      },
      type: {
        input: { value: 'Venda', required: true },
        label: { title: 'Tipo de Transação', className: 'col s4' },
      },
      value: {
        input: { value: '', placeholder: 'Digite o valor do Imóvel' },
        label: { title: 'Valor do Imóvel', className: 'col s5' },
      },
    };
    this.state = { form: this.emptyForm };

    this.newForm = this.newForm.bind(this);
    this.submit = this.submit.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    const { form } = this.state;
    form[e.target.name].input.value = e.target.value;
    this.setState({ form });
  }

  getFooter() {
    return (
      <React.Fragment>
        <ButtonReject type="button" onClick={this.props.onClose(false)}>Cancelar</ButtonReject>
        <ButtonConfirm marginLeft="10px" type="submit">Cadastrar</ButtonConfirm>
      </React.Fragment>
    );
  }

  submit(e) {
    if (
      this.state.form.title.input.value
      && this.state.form.location.input.value
      && this.state.form.type.input.value
    ) {
      const values = Object.keys(this.state.form).reduce((previous, current) => {
        previous[current] = this.state.form[current].input.value;
        return previous;
      }, {});
      this.props.onClose(values)();
    } else {
      alert('Favor Preencha todos os campos!');
    }
  }

  newForm() {
    this.setState({ form: this.emptyForm });
    document.getElementById('immobile-title').focus();
  }

  render() {
    return (
      <ModalPat
        isOpen={this.props.isOpen}
        onAfterOpen={this.newForm}
        onRequestClose={this.props.onClose(false)}
        onSubmit={this.submit}
        shouldCloseOnEsc={false}
        title="Novo Imóvel"
        footer={this.getFooter()}
      >
        <FormGroup fields={this.state.form} prefix="immobile-" onChange={this.onChange} />
      </ModalPat>
    );
  }
}

Immobile.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default Immobile;
