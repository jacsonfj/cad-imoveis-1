import React from 'react';
import { Switch, Route } from 'react-router-dom';
import styled from 'styled-components';

import './App.css';
import Sidebar from './components/SideBar';
import Home from './pages/Home';
import Imoveis from './pages/Imoveis';

const Main = styled.main.attrs({ className: 'full-wd col s13' })`
  flex-direction: column;
  justify-content: flex-start;
`;

const App = () => (
  <div className="col s16">
    <Sidebar />
    <Main>
      <Switch>
        <Route path="/imoveis" component={Imoveis} />
        <Route component={Home} />
      </Switch>
    </Main>
  </div>
);

export default App;
