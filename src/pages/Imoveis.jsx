import React from 'react';
import Axios from 'axios';

import Header from '../components/Header';
import CardImmobbile from '../components/CardImmobile';
import Immobile from '../forms/Immobile';

class Imoveis extends React.Component {
  constructor() {
    super();
    this.state = { immobiles: [], modalIsOpen: false };
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);

    Axios.get('http://localhost:3000/imoveis.json')
      .then(response => this.setState({ immobiles: response.data.data }))
      .catch(() => alert('Falha de comunicação com o servidor'));
  }

  openModal() {
    this.setState({ modalIsOpen: true });
  }
  closeModal(immo) {
    return () => {
      this.setState({ modalIsOpen: false });
      if (immo) {
        const { immobiles } = this.state;
        immobiles.push(immo);
        immobiles[immobiles.length - 1].id = immobiles.length;
        this.setState({ immobiles });
      }
    };
  }

  render() {
    return (
      <section>
        <Immobile isOpen={this.state.modalIsOpen} onClose={this.closeModal} />
        <Header title="Novo Imóvel" btn={{ title: 'Novo Imóvel', onClick: this.openModal }} />
        <div className="col s16">
          {this.state.immobiles.map(imo => (
            <CardImmobbile {...imo} key={imo.id} />
          ))}
        </div>
      </section>
    );
  }
}

export default Imoveis;
