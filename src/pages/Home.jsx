import React from 'react';

import Header from '../components/Header';

const Home = () => (
  <section>
    <Header title="Início" />
    <h1>Bem vindo</h1>
  </section>
);

export default Home;
